<?php get_header(); ?>

<div id="single-posts">
	<div class="clear">
		     <?php if (have_posts()) : ?>
		               <?php while (have_posts()) : the_post(); ?>  
		               
		               	<div class="post-container">  
			               
			               <div class="post-left about half">	
							<h1>About</h1>
							<div class="caption"><p><?php the_field('about_caption'); ?></p></div>
							<p><?php the_field('about'); ?></p>
							<!--<a class="button" href="#">Contact</a>-->
					
								
			               </div>
			               <div id="contact" class="contact half">
				               <h1>Contact</h1>
				               <div class="caption"><p><?php the_field('contact_caption'); ?></p></div>
				               <p>p: <?php the_field('phone'); ?></p>
				               <a href="mailto:<?php the_field('email'); ?>">e: <?php the_field('email') ?></a>
					           <div class="clear"></div>
					           <?php echo do_shortcode('[contact-form-7 id="9" title="Oz Stuidos"]'); ?>
				              <!-- <div class="post-share">		
					               <p>Share this post</p>	 
					               
					               <a href="https://twitter.com/share?url=&text=<?php the_title(); ?>: <?php echo urlencode(get_permalink($post->ID)); ?> &via=username&count=horizontal" class="twitter"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/twitter-rev.svg" alt="twitter" /></a>           		
					               		<a href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&t=<?php the_title(); ?>" target="blank" class="facebook"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/facebook-rev.svg" alt="facebook" /></a>
							 </div>
							 <div class="clear"></div>
							 <a class="button" href="<?php echo home_url(); ?>">Back to Home</a>
				               	 
			               </div>-->
			               
		               	</div>
		               <?php endwhile; ?>
		     <?php endif; ?>
	</div>
</div>	
		
<?php get_footer(); ?>