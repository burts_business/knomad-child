<?php get_header('landing'); ?>	
</section>

<div class="thumbs-block">
	<div class="home-container thumbs clear">
		<a href="<?php echo home_url(); ?>/animation/">
			<div class="home-pillar">
				<h2>Animation</h2>
				<div class="info">
					<p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
					<a class="button" href="#">Read More</a>
				</div>
			</div>
		</a>
		<a href="<?php echo home_url(); ?>/legacy/">
			<div class="home-pillar">
				<h2>Legacy</h2>
				<div class="info">
					<p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
					<a class="button" href="#">Read More</a>
				</div>
			</div>
			<div class="background-images" style="background-image: url('http://oz.blinkltd.co.nz/wp-content/uploads/2015/05/IMG_2251-1024x768.jpg')">
			</div>
		</a>
		<a href="<?php echo home_url(); ?>/creatures/">
			<div class="home-pillar creature">
				<h2>Creatures</h2>
				<div class="info">
					<p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
					<a class="button" href="#">Read More</a>
				</div>
			</div>
			<div class="background-images one" style="background-image: url('http://oz.blinkltd.co.nz/wp-content/uploads/2015/05/Archaeopteryx_058-1024x682.jpg')">
			</div>
		</a>
		<a href="<?php echo home_url(); ?>/odds-and-sods/">
			<div class="home-pillar">
				<h2>Odds and Sods</h2>
				<div class="info">
					<p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
					<a class="button" href="#">Read More</a>
				</div>
			</div>
		</a>
		<a href="<?php echo home_url(); ?>/scale/">
			<div class="home-pillar">
				<h2>Scale</h2>
				<div class="info">
					<p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
					<a class="button" href="#">Read More</a>
				</div>
			</div>
		</a>
	</div>
	<div id="testdiv"></div>
</div>

<?php get_footer('landing'); ?>