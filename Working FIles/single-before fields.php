<?php get_header(); ?>

<div id="single-posts">
	<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>  
		               
		<div class="post-container clear">            
			<div class="post-left half">	
				<h1 class="mobile-header"><?php the_title();?></h1>
				    <div class="wrapper">
						<div id="page-1" class="scrilla"> 
					    	<ul class="slider">
								<li>
									<div class="flexslider">
										<ul class="slides">
											<li>
												<div class="image-placeholder"></div>
											</li>
											<li>
												<div class="image-placeholder dark"></div>
											</li>
											<li>
												<div class="image-placeholder"></div>
											</li>
										</ul>
									</div>
								
								
									<div class="details">
										<svg version="1.1" id="cross" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
										viewBox="0 0 38.974 38.667" enable-background="new 0 0 38.974 38.667" xml:space="preserve">
										<g>
											<circle fill="none" stroke="#58595B" stroke-width="1.5" stroke-miterlimit="10" cx="19.757" cy="19.282" r="17.526"/>
											<line class="vert" fill="none" stroke="#58595B" stroke-width="1.5" stroke-miterlimit="10" x1="19.757" y1="9.971" x2="19.757" y2="29.689"/>
											<line class="horiz" fill="none" stroke="#58595B" stroke-width="1.5" stroke-miterlimit="10" x1="9.898" y1="19.83" x2="29.616" y2="19.83"/>
										</g>
										</svg>
								
										<div class="open">Expand for more details</div>
										<div class="display clear">
											<div class="third">
												<h3>Project:</h3>
												<p>Karen Walker Lookbook</p>
											</div>
											<div class="two-thirds">
												<h3>Details:</h3>
												<p>Maecenas sed diam eget risus varius blandit sit amet non magna. Sed posuere consectetur est at lobortis. Etiam porta sem malesuada magna mollis euismod. Donec id elit non mi porta gravida at eget metus. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
											</div>
										</div>
									</div>
								</li>
							</ul>
 
					    </div>
					    <div id="page-2" class="scrilla">  
					    
					    <ul class="slider">
								<li>
									<div class="flexslider">
										<ul class="slides">
											<li>
												<div class="image-placeholder"></div>
											</li>
											<li>
												<div class="image-placeholder dark"></div>
											</li>
											<li>
												<div class="image-placeholder"></div>
											</li>
										</ul>
									</div>
								
								
									<div class="details">
										<svg version="1.1" id="cross" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
										viewBox="0 0 38.974 38.667" enable-background="new 0 0 38.974 38.667" xml:space="preserve">
										<g>
											<circle fill="none" stroke="#58595B" stroke-width="1.5" stroke-miterlimit="10" cx="19.757" cy="19.282" r="17.526"/>
											<line class="vert" fill="none" stroke="#58595B" stroke-width="1.5" stroke-miterlimit="10" x1="19.757" y1="9.971" x2="19.757" y2="29.689"/>
											<line class="horiz" fill="none" stroke="#58595B" stroke-width="1.5" stroke-miterlimit="10" x1="9.898" y1="19.83" x2="29.616" y2="19.83"/>
										</g>
										</svg>
								
										<div class="open">Expand for more details</div>
										<div class="display clear">
											<div class="third">
												<h3>Project:</h3>
												<p>Karen Walker Lookbook</p>
											</div>
											<div class="two-thirds">
												<h3>Details:</h3>
												<p>Maecenas sed diam eget risus varius blandit sit amet non magna. Sed posuere consectetur est at lobortis. Etiam porta sem malesuada magna mollis euismod. Donec id elit non mi porta gravida at eget metus. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
											</div>
										</div>
									</div>
								</li>
							</ul>
 
					        
					    </div>
					    <div id="page-3" class="scrilla"> 
					    </div>
					   
								    
					</div>
					<div class="clear">
						<div class="third">
						   <a href="#page-1" class="scrollitem selected"><div class="image-placeholder dark short"></div></a>
						</div>
						<div class="third">
						   <a href="#page-2" class="scrollitem"><div class="image-placeholder short"></div></a>
						</div>
						<div class="third">
						   <a href="#page-3" class="scrollitem"><div class="image-placeholder dark short"></div></a>
						</div>
					</div>
					 
			    </div>
				<div class="post-right half">
					<h1 class="desktop-header"><?php the_title();?></h1>
					<div class="caption">
						<p><?php the_field('caption'); ?></p>
					</div>
					<?php the_content();?>
					<a class="button" href="/contact/">Contact</a> 
				</div>
			               
		    </div>
		<?php endwhile; ?>
		<?php endif; ?>
	</div>
</div>	
		
<?php get_footer(); ?>