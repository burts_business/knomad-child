	</section>
	
	<footer id="footer" class="clear">
		<hr>
		<div class="footer-content">
			<div class="quarter">
				<a href="<?php echo home_url(); ?>">Oz Studios</a>
			</div>
			<div class="quarter">
				<a href="/contact/"><p>Contact</p></a>
			</div>
			<div class="quarter">
				&nbsp;
			</div>
			<div class="quarter text-right">
				<p>&#169; Copyright Oz Studios. <?php echo date("Y") ?></p>
				<a href="http://blinkltd.co.nz/" target="_blank">
					handmade by blink.
				</a>
			</div>
		</div>
	</footer>
	
	<a class="exit-off-canvas"></a>

  </div><!--innnerwrap-->
</div><!--off canvas wrap-->

  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="js/libs/jquery-1.7.min.js">\x3C/script>')</script>
  
  <script>
//	$('.prv, .nxt').click(function(e){
//	  $(this).siblings('.slider').stop().animate({
//	    scrollLeft: (this.className.match('nxt')?'+=':'-=')+300
//	  },1000); 
//	});
  </script>
  
  
  <script>
  $(document).ready(function () {
	    var sildeNum = $('.scrilla').length,
	        wrapperWidth = 100 * sildeNum,
	        slideWidth = 100/sildeNum;
	    $('.wrapper').width(wrapperWidth + '%'); 
	    $('.scrilla').width(slideWidth + '%');
	    
	    $('a.scrollitem').click(function(){
	        $('a.scrollitem').removeClass('selected');
	        $(this).addClass('selected');
	        
	        var slideNumber = $($(this).attr('href')).index('.scrilla'),
	            margin = slideNumber * -100 + '%';
	    
	        $('.wrapper').animate({marginLeft: margin},1000);
	        return false;
	    });
	});
	</script>


  <!-- FlexSlider -->
  <script defer src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.flexslider.js"></script>

  <script type="text/javascript">
    $(function(){
      SyntaxHighlighter.all();
    });
    $(window).load(function(){
      $('.flexslider').flexslider({
        animation: "slide",
        slideshow: true, 
        slideshowSpeed: 4000,           //Integer: Set the speed of the slideshow cycling, in milliseconds
        start: function(slider){
          $('body').removeClass('loading');
        }
      });
    });
  </script>
 
  
  <!-- Syntax Highlighter -->
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/flex/shCore.js"></script>
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/flex/shBrushXml.js"></script>
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/flex/shBrushJScript.js"></script>

  <!-- Optional FlexSlider Additions -->
  <script src="<?php bloginfo('stylesheet_directory'); ?>/js/flex/jquery.easing.js"></script>
  <script src="<?php bloginfo('stylesheet_directory'); ?>/js/flex/jquery.mousewheel.js"></script>
  <script defer src="<?php bloginfo('stylesheet_directory'); ?>/js/flex/demo.js"></script>
  
  

    </script>
    
    <script type="text/javascript">
    var userFeed = new Instafeed({
        get: 'user',
        userId: 292435602,
        accessToken: '16498346.467ede5.d26ab444ea9741ffbe8ddff4c549ef91',
        resolution: 'standard_resolution',
		limit: 4
    });
    userFeed.run();
</script>
   
    
  
<?php wp_footer(); ?>
</body>
</html>