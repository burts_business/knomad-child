<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Oz Studios | Model Maker Extraodanier</title>
    
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/app.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/flexslider.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/style.css" type="text/css"/>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/typography.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/media.css" />
   <!--Novecento-Wide-->
   	<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/stylesheet.css" type="text/css" charset="utf-8" />

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js" type="text/javascript"></script>
	
	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/instafeed.min.js"></script>
	
	<script src="<?php bloginfo('stylesheet_directory'); ?>/js/brooke.js"></script>
	
	<script src="//use.typekit.net/gah8hly.js"></script>
	<script>try{Typekit.load();}catch(e){}</script>
	
	<script>
//	$(document).ready(function () {
//    $('.expand').hide().before('<p id="expand">Expand for more details</p>');
//	    $('p#expand').click(function () {
//	        $('.expand').slideToggle(200);
//	        return false;
//		    });
//	});
	
	$(document).ready(function () {
    $('.open').click(function () {
        var link = $(this);
        $('.display').slideToggle(200, function () {
            if ($(this).is(":visible")) {
                link.text('Hide for less');
            } else {
                link.text('Expand for more details');
            }
        });

    });

});
	</script>
	<script>
		$(function(){
  
		    var $bl    = $(".thumbs-block"),
		        $th    = $(".thumbs"),
		        blW    = $bl.outerWidth(),
		        blSW   = $bl[0].scrollWidth,
		        wDiff  = (blSW/blW)-1,  // widths difference ratio
		        mPadd  = 60,  // Mousemove Padding
		        damp   = 20,  // Mousemove response softness
		        mX     = 0,   // Real mouse position
		        mX2    = 0,   // Modified mouse position
		        posX   = 0,
		        mmAA   = blW-(mPadd*2), // The mousemove available area
		        mmAAr  = (blW/mmAA);    // get available mousemove fidderence ratio
		
			$bl.mousemove(function(e) {
		        mX = e.pageX - this.offsetLeft;
		        mX2 = Math.min( Math.max(0, mX-mPadd), mmAA ) * mmAAr;
			});
		
			setInterval(function(){
				posX += (mX2 - posX) / damp; // zeno's paradox equation "catching delay"	
				$th.css({marginLeft: -posX*wDiff });
			}, 10);
		
		});


	</script>



    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>

  <div class="off-canvas-wrap">
	  <div class="inner-wrap">  

		
		<header class="header-home mobile">			
				
			<nav class="left-menu">
				<a href="<?php echo home_url(); ?>/about/">About</a>
			</nav>
				<div class="logo">
					<a href="<?php echo home_url(); ?>"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/logo.svg" alt="Oz Studios" /></a>
				</div>
			<nav class="right-menu">
				<a class="nav-right" href="<?php echo home_url(); ?>/about/#contact/">Contact</a>
			</nav> 
			
			<img class="menu-small" src="<?php bloginfo('stylesheet_directory'); ?>/images/menu.svg" alt="Menu"/>

		</header>

					
		<section id="main-content" role="document">