<?php
/*
Template Name: Film
*/
get_header(); ?>

<div id="single-posts" class="container">
	<div class="clear">
		<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?>  
			
			<div class="post-container music-video">  
			
				<div class="post-left">	
					<div class="embed-container">
						<?php the_field('video_embed'); ?>
					</div>
				</div>
				<div class="post-right">
					<div class="info info-left">
						<h2><span class="highlight"><?php the_title();?></span></h2>
						<?php if( get_field('year') ): ?>
							<p>(<?php the_field('year'); ?>)</p>
						<?php endif; ?>
						<p><?php the_field('role'); ?></p>
					</div>
					<div class="info info-right">
						<p><?php the_tags(); ?></p>
					</div>
					<div class="clearfix"></div>
					<div class="copy">    
						<p><?php the_field('body_copy');?></p>
						
						
						<?php if( get_field('additional_credits') ): ?>

							<p>Additional Credits:</p>
							<p class="credits"><?php the_field('additional_credits'); ?></p>

						<?php endif; ?>
						
						
					</div>
			
			</div>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
</div>	

<?php get_footer(); ?>