<?php
/*
Template Name: Videos
*/
get_header(); ?>
<div class="background-clear">

<div id="videos" class="container">

<?php $args = array( 'post_type' => 'albums');
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post();?>
  <h2 class="<?php the_field('contrast'); ?>"><?php the_title(); ?>(<?php the_field('year'); ?>)</h2>

<div class="album-tracks clear">
<?php
 
// check if the repeater field has rows of data
if( have_rows('track_listing') ):
 
 	// loop through the rows of data
    while ( have_rows('track_listing') ) : the_row();?>
 
 <?php 
								$value = get_sub_field('video');

								//var_dump($value);

								if ($value == "") {
									echo "";
								}else{ 	?>
								
    <li class="track-listing column">
					<ul>
		
						<li class="play-video"  data-youtube="<?php the_sub_field('video'); ?>">

								<ul>
								<li class="left">
									<?php the_sub_field('track_name'); ?>
								</li>
								<li>
									<?php	
									echo "Play";
								}
							?>
								</li>
								</ul>
						</li>

					</ul>
			</li>
			<li class="youtube embed-container <?php the_sub_field('video'); ?>">
				
			</li>

 
  <?php  endwhile;?>
</div>
<?else :
 
    // no rows found
 
endif;


endwhile; ?>

</div>	
</div>
		
<?php get_footer(); ?>