<?php
/*
Template Name: Tracks
*/
get_header(); ?>
<div class="background-clear">
<div id="tracks" class="container">

<?php $args = array( 'post_type' => 'albums');
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post();?>
  <h2 class="<?php the_field('contrast'); ?>"><?php the_title(); ?>(<?php the_field('year'); ?>)</h2>
  <?php echo '<div class="album-info">';

 
 $image = get_field('album_image');
 
if( !empty($image) ): ?>
 
	<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
 
<?php endif;
  echo '</div>';?>

<div class="album-tracks clear">
<?php
 
// check if the repeater field has rows of data
if( have_rows('track_listing') ):
 
 	// loop through the rows of data
    while ( have_rows('track_listing') ) : the_row();?>
 
    <li class="track-listing column">
				<div class="player-controls">
					<ul>
						<li>	
							<button>
								<span class="icon-play"><img class="play" src="http://knomad.co.nz/images/icon-play-black.svg" alt="play" data-itunes="<?php the_sub_field('itunes_link'); ?>"/></span><span class="icon-pause"><img class="pause" src="http://knomad.co.nz/images/icon-pause.svg" alt="pause"/></span>
								<span class="play" data-itunes="<?php the_sub_field('itunes_link'); ?>"><p><?php the_sub_field('track_name'); ?></p></span>
							</button>
							
						</li>
					</ul>	
				</div>
				<div class="track-meta">
					<ul>
						<li class="purchase-track">
							<a href="<?php the_sub_field('purchase_link'); ?>" target="itunes_store">Buy</a>
						</li>
						<!--<li class="track-duration">
							(<?php the_sub_field('duration'); ?>)
						</li>-->
						<!--<li class="lyrics">
							<a href="<?php the_sub_field('lyrics'); ?>">Lyrics</a>
						</li>-->
						<li class="play-video"  data-youtube="<?php the_sub_field('video'); ?>">

							<?php 
								$value = get_sub_field('video');

								//var_dump($value);

								if ($value == "") {
									echo "";
								}else{
									echo "Video";
								}
							?>
						</li>

					</ul>
				</div>
			</li>
			
			<li class="youtube embed-container <?php the_sub_field('video'); ?>">
			</li>
			<div class="soundcloud-container">
			<?php
		 
				// check if the repeater field has rows of data
				if( have_rows('soundcloud_tracks') ):?>
			 
			 	<?php while ( have_rows('soundcloud_tracks') ) : the_row(); ?>
	
				<div class="soundcloud"><?php the_sub_field('track_link'); ?></div>
				
				<?php endwhile;?>
				<?php endif;?>
				
			 
			</div>	 
			
 
  <?php  endwhile;?>
</div>
<div class="clear"></div>
<?else :
 
    // no rows found
 
endif;


endwhile; ?>

</div>	
</div>
<?php get_footer(); ?>