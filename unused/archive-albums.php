<?php get_header(); ?>
<div class="background-clear" style="paading-top:20px;">
<div id="archives" class="container">
	<div id="top-section">
		<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?>    
			<div class="album-slider">
				<?php
				$image = get_field('album_image');	 
				if( !empty($image) ): ?>
					<a href="<?php the_permalink(); ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
				<?php endif; ?>
				<div class="album-slider-hover">
				<a href="<?php the_permalink(); ?>"><h1><?php the_title();?></h1></a>
				</div>
			</div>			               
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
</div>	
</div>
		
<?php get_footer(); ?>
