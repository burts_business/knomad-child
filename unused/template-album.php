<?php
/*
Template Name: Album
*/
get_header(); ?>

<div class="container">
	<div id="top-section">
		<div id="album-slider">
			<img src="<?php bloginfo('stylesheet_directory'); ?>/images/flags.jpg" alt="brooke-main"/>
			<h1>Flags</h1>
		</div>	
	</div>
	<div class="album-tracks clear">
		<ul>
			<li class="track-listing column">
				<div class="player-controls">
					<ul>
						<li>	
							<button>
								<span class="icon-play"><img class="play" src="http://knomad.co.nz/images/icon-play-black.svg" alt="play" data-itunes="http://a171.phobos.apple.com/us/r1000/093/Music/4f/95/3c/mzm.mpdkgihj.aac.p.m4a"/></span><span class="icon-pause"><img class="pause" src="http://knomad.co.nz/images/icon-pause.svg" alt="pause"/></span>
								Something in the Water
							</button>
							
						</li>
					</ul>	
				</div>
				<div class="track-meta">
					<ul>
						<li class="purchase-track">
							<a href="https://itunes.apple.com/nz/album/something-in-the-water/id395080424?i=395080425&uo=4" target="itunes_store">Buy</a>
						</li>
						<li class="track-duration">
							(3:03)
						</li>
						<li class="lyrics">
							Lyrics
						</li>
						<li class="play-video">
							Video
						</li>		
					</ul>
				</div>
			</li>
			<li class="track-listing column">
				<div class="player-controls">
					<ul>
						<li>	
							<button>
								<span class="icon-play"><img class="play" src="http://knomad.co.nz/images/icon-play-black.svg" alt="play" data-itunes="http://a555.phobos.apple.com/us/r1000/091/Music/v4/d0/77/98/d077981d-5444-f9c8-9897-4135246d6de8/mzaf_4449046756860428436.aac.m4a"/></span><span class="icon-pause"><img class="pause" src="http://knomad.co.nz/images/icon-pause.svg" alt="pause"/></span>
								Betty
							</button>
							
						</li>
					</ul>	
				</div>
				<div class="track-meta">
					<ul>
						<li class="purchase-track">
							<a href="https://itunes.apple.com/nz/album/flags/id395080424#" target="itunes_store">Buy</a>
						</li>
						<li class="track-duration">
							(2:58)
						</li>
						<li class="lyrics">
							Lyrics
						</li>
						<li class="play-video">
							Video
						</li>		
					</ul>
				</div>
			</li>
			<li class="track-listing column">
				<div class="player-controls">
					<ul>
						<li>	
							<button>
								<span class="icon-play"><img class="play" src="http://knomad.co.nz/images/icon-play-black.svg" alt="play" data-itunes="http://a1397.phobos.apple.com/us/r1000/097/Music/v4/c8/38/a1/c838a149-95e6-9f1d-7d2c-c4c1fd5563c4/mzaf_2355121992503212831.aac.m4a"/></span><span class="icon-pause"><img class="pause" src="http://knomad.co.nz/images/icon-pause.svg" alt="pause"/></span>
								Orphans, Kingdoms
							</button>
							
						</li>
					</ul>	
				</div>
				<div class="track-meta">
					<ul>
						<li class="purchase-track">
							<a href="https://itunes.apple.com/nz/album/flags/id395080424#" target="itunes_store">Buy</a>
						</li>
						<li class="track-duration">
							(3:45)
						</li>
						<li class="lyrics">
							Lyrics
						</li>
						<li class="play-video">
							Video
						</li>		
					</ul>
				</div>
			</li>
			<li class="track-listing column">
				<div class="player-controls">
					<ul>
						<li>	
							<button>
								<span class="icon-play"><img class="play" src="http://knomad.co.nz/images/icon-play-black.svg" alt="play" data-itunes="http://a770.phobos.apple.com/us/r1000/061/Music2/v4/98/1d/8a/981d8a1c-e926-8339-ae2f-3bcc52d28387/mzaf_4322772055227195946.aac.m4a"/></span><span class="icon-pause"><img class="pause" src="http://knomad.co.nz/images/icon-pause.svg" alt="pause"/></span>
								Who Are We Fooling (feat. Aqualung)
							</button>
							
						</li>
					</ul>	
				</div>
				<div class="track-meta">
					<ul>
						<li class="purchase-track">
							<a href="https://itunes.apple.com/nz/album/flags/id395080424#" target="itunes_store">Buy</a>
						</li>
						<li class="track-duration">
							(4:25)
						</li>
						<li class="lyrics">
							Lyrics
						</li>
						<li class="play-video">
							Video
						</li>		
					</ul>
				</div>
			</li>
			<li class="track-listing column">
				<div class="player-controls">
					<ul>
						<li>	
							<button>
								<span class="icon-play"><img class="play" src="http://knomad.co.nz/images/icon-play-black.svg" alt="play" data-itunes="http://a961.phobos.apple.com/us/r1000/080/Music2/v4/21/bf/d0/21bfd0b9-fb33-29ea-7bed-3413efd794e5/mzaf_3436696285120291000.aac.m4a"/></span><span class="icon-pause"><img class="pause" src="http://knomad.co.nz/images/icon-pause.svg" alt="pause"/></span>
								Ice On Her Lashes
							</button>
							
						</li>
					</ul>	
				</div>
				<div class="track-meta">
					<ul>
						<li class="purchase-track">
							<a href="https://itunes.apple.com/nz/album/flags/id395080424#" target="itunes_store">Buy</a>
						</li>
						<li class="track-duration">
							(5:44)
						</li>
						<li class="lyrics">
							Lyrics
						</li>
						<li class="play-video">
							Video
						</li>		
					</ul>
				</div>
			</li>
			<li class="track-listing column">
				<div class="player-controls">
					<ul>
						<li>	
							<button>
								<span class="icon-play"><img class="play" src="http://knomad.co.nz/images/icon-play-black.svg" alt="play" data-itunes="http://a446.phobos.apple.com/us/r1000/117/Music/v4/35/76/b0/3576b056-6ca4-8536-d129-033804dd13e6/mzaf_8065871278338530588.aac.m4a"/></span><span class="icon-pause"><img class="pause" src="http://knomad.co.nz/images/icon-pause.svg" alt="pause"/></span>
								Coachella
							</button>
							
						</li>
					</ul>	
				</div>
				<div class="track-meta">
					<ul>
						<li class="purchase-track">
							<a href="https://itunes.apple.com/nz/album/flags/id395080424#" target="itunes_store">Buy</a>
						</li>
						<li class="track-duration">
							(3:32)
						</li>
						<li class="lyrics">
							Lyrics
						</li>
						<li class="play-video">
							Video
						</li>		
					</ul>
				</div>
			</li>
			<li class="track-listing column">
				<div class="player-controls">
					<ul>
						<li>	
							<button>
								<span class="icon-play"><img class="play" src="http://knomad.co.nz/images/icon-play-black.svg" alt="play" data-itunes="http://a1785.phobos.apple.com/us/r1000/092/Music/v4/f4/6f/58/f46f589f-c9a7-b7d5-5b02-afe9dea10774/mzaf_1255847804656899737.aac.m4a"/></span><span class="icon-pause"><img class="pause" src="http://knomad.co.nz/images/icon-pause.svg" alt="pause"/></span>
								Jack Kerouac
							</button>
							
						</li>
					</ul>	
				</div>
				<div class="track-meta">
					<ul>
						<li class="purchase-track">
							<a href="https://itunes.apple.com/nz/album/flags/id395080424#" target="itunes_store">Buy</a>
						</li>
						<li class="track-duration">
							(3:25)
						</li>
						<li class="lyrics">
							Lyrics
						</li>
						<li class="play-video">
							Video
						</li>		
					</ul>
				</div>
			</li>
			<li class="track-listing column">
				<div class="player-controls">
					<ul>
						<li>	
							<button>
								<span class="icon-play"><img class="play" src="http://knomad.co.nz/images/icon-play-black.svg" alt="play" data-itunes="http://a583.phobos.apple.com/us/r1000/068/Music/v4/dd/f4/b8/ddf4b88d-89e8-bd42-1a98-3bd746323b25/mzaf_777009571478417160.aac.m4a"/></span><span class="icon-pause"><img class="pause" src="http://knomad.co.nz/images/icon-pause.svg" alt="pause"/></span>
								Sailboats
							</button>
							
						</li>
					</ul>	
				</div>
				<div class="track-meta">
					<ul>
						<li class="purchase-track">
							<a href="https://itunes.apple.com/nz/album/flags/id395080424#" target="itunes_store">Buy</a>
						</li>
						<li class="track-duration">
							(3:18)
						</li>
						<li class="lyrics">
							Lyrics
						</li>
						<li class="play-video">
							Video
						</li>		
					</ul>
				</div>
			</li>
			<li class="track-listing column">
				<div class="player-controls">
					<ul>
						<li>	
							<button>
								<span class="icon-play"><img class="play" src="http://knomad.co.nz/images/icon-play-black.svg" alt="play" data-itunes="http://a118.phobos.apple.com/us/r1000/076/Music2/v4/93/f7/03/93f703d8-48bd-69b7-facd-416d8bbbf458/mzaf_7246514485085183329.aac.m4a"/></span><span class="icon-pause"><img class="pause" src="http://knomad.co.nz/images/icon-pause.svg" alt="pause"/></span>
								Crows & Locusts
							</button>
							
						</li>
					</ul>	
				</div>
				<div class="track-meta">
					<ul>
						<li class="purchase-track">
							<a href="https://itunes.apple.com/nz/album/flags/id395080424#" target="itunes_store">Buy</a>
						</li>
						<li class="track-duration">
							(5:47)
						</li>
						<li class="lyrics">
							Lyrics
						</li>
						<li class="play-video">
							Video
						</li>		
					</ul>
				</div>
			</li>
			<li class="track-listing column">
				<div class="player-controls">
					<ul>
						<li>	
							<button>
								<span class="icon-play"><img class="play" src="http://knomad.co.nz/images/icon-play-black.svg" alt="play" data-itunes="http://a1793.phobos.apple.com/us/r1000/085/Music2/v4/35/74/8e/35748e02-2f5a-1616-b190-dd3b02d2cec6/mzaf_290962691179842696.aac.m4a"/></span><span class="icon-pause"><img class="pause" src="http://knomad.co.nz/images/icon-pause.svg" alt="pause"/></span>
								Here's to You
							</button>
							
						</li>
					</ul>	
				</div>
				<div class="track-meta">
					<ul>
						<li class="purchase-track">
							<a href="https://itunes.apple.com/nz/album/flags/id395080424#" target="itunes_store">Buy</a>
						</li>
						<li class="track-duration">
							(4:22)
						</li>
						<li class="lyrics">
							Lyrics
						</li>
						<li class="play-video">
							Video
						</li>		
					</ul>
				</div>
			</li>
			<li class="track-listing column">
				<div class="player-controls">
					<ul>
						<li>	
							<button>
								<span class="icon-play"><img class="play" src="http://knomad.co.nz/images/icon-play-black.svg" alt="play" data-itunes="http://a1539.phobos.apple.com/us/r1000/093/Music2/v4/b2/3d/d5/b23dd5a2-d8c9-4eac-2bdb-0b14d79d0884/mzaf_7787353548224666988.aac.m4a"/></span><span class="icon-pause"><img class="pause" src="http://knomad.co.nz/images/icon-pause.svg" alt="pause"/></span>
								Flags
							</button>
							
						</li>
					</ul>	
				</div>
				<div class="track-meta">
					<ul>
						<li class="purchase-track">
							<a href="https://itunes.apple.com/nz/album/flags/id395080424#" target="itunes_store">Buy</a>
						</li>
						<li class="track-duration">
							(4:46)
						</li>
						<li class="lyrics">
							Lyrics
						</li>
						<li class="play-video">
							Video
						</li>		
					</ul>
				</div>
			</li>
			<li class="track-listing column">
				<div class="player-controls">
					<ul>
						<li>	
							<button>
								<span class="icon-play"><img class="play" src="http://knomad.co.nz/images/icon-play-black.svg" alt="play" data-itunes="http://a1842.phobos.apple.com/us/r1000/117/Music2/v4/04/78/de/0478de6d-f708-b4f1-0685-4a53ba89f67d/mzaf_7077734375659749059.aac.m4a"/></span><span class="icon-pause"><img class="pause" src="http://knomad.co.nz/images/icon-pause.svg" alt="pause"/></span>
								You Can Close Your Eyes (feat. William Fitzsimmons)
							</button>
							
						</li>
					</ul>	
				</div>
				<div class="track-meta">
					<ul>
						<li class="purchase-track">
							<a href="https://itunes.apple.com/nz/album/flags/id395080424#" target="itunes_store">Buy</a>
						</li>
						<li class="track-duration">
							(2:22)
						</li>
						<li class="lyrics">
							Lyrics
						</li>
						<li class="play-video">
							Video
						</li>		
					</ul>
				</div>
			</li>
		</ul>   		        
	</div>
</div>	
		
<?php get_footer(); ?>