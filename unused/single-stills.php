
<?php
/*
Template Name: Stills
*/
get_header(); ?>

<div id="single-posts" class="container">
	<div class="clear">
		<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?>  
			
			<div class="post-container music-video">  
			
				<div class="post-left">	
				
					<?php $images = get_field('gallery');
						
						if( $images ): ?>
						    <div id="slider" class="flexslider">
						        <ul class="slides">
						            <?php foreach( $images as $image ): ?>
						                <li data-thumb="<?php echo $image ['url']; ?>">
						                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						                    <p><?php echo $image['caption']; ?></p>
						                </li>
						            <?php endforeach; ?>
						        </ul>
						    </div>
						    <!--<div id="carousel" class="flexslider">
						        <ul class="slides">
						            <?php foreach( $images as $image ): ?>
						                <li>
						                    <img width="50px" src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
						                </li>
						            <?php endforeach; ?>
						        </ul>
						    </div>-->
						<?php endif; ?>
					
				</div>
				<div class="post-right">
					<div class="info info-left">
						<h2><span class="highlight"><?php the_title();?></span></h2>
						<p>(<?php the_field('year'); ?>)</p>
						<!--<p><?php the_field('role'); ?></p>-->
					</div>
					<div class="info info-right">
						<p>Billabong, Something, Something Else</p>
					</div>
					<div class="clearfix"></div>
					<div class="copy">    
						<p><?php the_field('body_copy');?></p>
						<?php if( get_field('additional_credits') ): ?>

							<p>Additional Credits:</p>
							<p class="credits"><?php the_field('additional_credits'); ?></p>

						<?php endif; ?>
						

					</div>
			
			</div>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
</div>	

<?php get_footer(); ?>