<?php 
/*
Template Name: Home
*/get_header(); ?>	

	
	 <?php $args = array( 'post_type' => 'post');
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post();?>
		<div class="third main-post">
			<a href="<?php the_permalink(); ?>">
				<img width="500" height="280" src="<?php the_field('placeholder_image'); ?>" 
				class="attachment-medium-post wp-post-image" alt="surfing" />
			
				<p style="font-style:italic;"><?php the_title(); ?></p>
				<div class="video-preview">
					   
				    <video autoplay muted src="<?php the_field('preview_video'); ?>" loop="" poster="<?php the_field('placeholder_image'); ?>"></video>
	
					 <!--<video autoplay src="http://whatyouth.com/wp-content/uploads/2015/02/HA_austincalvello_VT.mp4" loop="" poster="http://whatyouth.com/wp-content/uploads/2015/02/homeave_photo-500x280.jpg"></video>-->

				</div>
			</a>
	    </div>
  <?php  endwhile;?>

<?php get_footer(); ?>