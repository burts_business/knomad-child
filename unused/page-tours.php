<?php get_header(); ?>
<div class="tour-background">
	<div class="container">
		<h1>Tours</h1>
		<div class="post-share">	
       		<p>Share this post</p>	               		
       		<a href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&t=<?php the_title(); ?>" target="blank" class="facebook"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/facebook.svg" alt="facebook" /></a>
       		<a href="https://twitter.com/share?url=&text=<?php the_title(); ?>: <?php echo urlencode(get_permalink($post->ID)); ?> &via=username&count=horizontal" class="twitter"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/twitter.svg" alt="twitter" /></a>
	    </div>
		<script type='text/javascript' src='http://widget.bandsintown.com/javascripts/bit_widget.js'></script><a href="http://www.bandsintown.com/Brooke%20Fraser" class="bit-widget-initializer" data-artist="Brooke Fraser">Brooke Fraser Tour Dates</a>
	</div>	
</div>
		
<?php get_footer(); ?>