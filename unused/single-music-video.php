<?php
/*
Template Name: Album
*/
get_header(); ?>

<div id="single-posts" class="container">
	<div class="clear">
		     <?php if (have_posts()) : ?>
		               <?php while (have_posts()) : the_post(); ?>  
		               
		               	<div class="post-container music-video">  
			               
			               <div class="post-left">	
				              <style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='<?php the_field('youtube_url'); ?>' frameborder='0' allowfullscreen></iframe></div>
			               </div>
			               <div class="post-right">
			               		<div class="copy">
					               <h2><span class="highlight"><?php the_title();?></span></h2>
					               <p><span class="highlight"><?php the_field('caption'); ?></span></p>
			               		</div>
			               		<div class="info">    
					               
					               <div class="clear"></div>
					                <a class="button" href="<?php echo home_url(); ?>">Back to Home</a>
					               <div class="post-share">	
					               	<p>Share this post</p>	               		
					               		<a href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&t=<?php the_title(); ?>" target="blank" class="facebook"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/facebook-rev.svg" alt="facebook" /</a>
					               		<a href="https://twitter.com/share?url=&text=<?php the_title(); ?>: <?php echo urlencode(get_permalink($post->ID)); ?> &via=username&count=horizontal" class="twitter"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/twitter-rev.svg" alt="twitter" /></a>
					               </div>
					              
				               	</div>
			               </div>
			               
		               	</div>
		               <?php endwhile; ?>
		     <?php endif; ?>
	</div>
</div>	
		
<?php get_footer(); ?>