<?php get_header(); ?>
<div class="background-clear">
<div class="container">
	<div id="top-section">
		<div class="album-slider">
			<?php 
 
$image = get_field('album_image');
 
if( !empty($image) ): ?>
 
	<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
 
<?php endif; ?>
			<h1 class="<?php the_field('class'); ?>"><?php the_title();?></h1>
		</div>
	</div>
	<div class="album-tracks clear">
		<ul>
		<?php
 
		// check if the repeater field has rows of data
		if( have_rows('track_listing') ):?>
		 
		 	<?php while ( have_rows('track_listing') ) : the_row(); ?>
		  
			<li class="track-listing column">
				<div class="player-controls">
					<ul>
						<li>	
							<button>
								<span class="icon-play"><img class="play" src="http://knomad.co.nz/images/icon-play-black.svg" alt="play" data-itunes="<?php the_sub_field('itunes_link'); ?>"/></span><span class="icon-pause"><img class="pause" src="http://knomad.co.nz/images/icon-pause.svg" alt="pause"/></span>
								<p><?php the_sub_field('track_name'); ?></p>
							</button>
							
						</li>
						<li id="video-one" class="youtube embed-container <?php the_sub_field('video'); ?>">
						</li>
					</ul>	
				</div>
				<div class="track-meta">
					<ul>
						<li class="purchase-track">
							<a href="<?php the_sub_field('purchase_link'); ?>" target="itunes_store">Buy</a>
						</li>
						<!--<li class="track-duration">
							(<?php the_sub_field('duration'); ?>)
						</li>-->
						<li class="lyrics">
							Lyrics
						</li>
						<li class="play-video"  data-youtube="<?php the_sub_field('video'); ?>">

							<?php 
								$value = get_sub_field('video');

								//var_dump($value);

								if ($value == "") {
									echo "";
								}else{
									echo "Video";
								}
							?>
						</li>
						<li class="lyrics-container">
							<?php the_sub_field('lyrics'); ?>
						</li>
					</ul>
				</div>
			</li>
			

			  <?php endwhile;
		 
		else :?>
		 
		<?php    // no rows found
		 
		endif;
		 
		?>
		</ul> 
		<ul class="soundcloud-container">
		<?php
 
		// check if the repeater field has rows of data
		if( have_rows('soundcloud_tracks') ):?>
		 
		 	<?php while ( have_rows('soundcloud_tracks') ) : the_row(); ?>

			<li class="soundcloud"><?php the_sub_field('track_link'); ?></li>
			
			  <?php endwhile;
		 
		else :?>
		 
		<?php    // no rows found
		 
		endif;
		?>
		</ul>	    	      
	</div>
</div>	
</div>
<?php get_footer(); ?>
