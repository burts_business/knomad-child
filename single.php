<?php get_header(); ?>

<div id="single-posts">
	<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>  
		               
		<div class="post-container clear">            
			<div class="post-left half">	
				<h1 class="mobile-header"><?php the_title();?></h1>
				    <div class="wrapper">
					    
					    <?php if( have_rows('project') ):
							
							 	// loop through the rows of data
							    while ( have_rows('project') ) : the_row();?>
							    
							    <div id="<?php the_sub_field('project_slug'); ?>" class="scrilla"> 
							    	<ul class="slider">
										<li>
											<div class="flexslider">
											    <ul class="slides">
												    
												    <?php if( get_sub_field('project_video') ): ?>
													<li>
														<div class="embed-container">
															<?php the_sub_field('project_video'); ?>
														</div>
													</li>
													<?php endif; ?>
													
												    <?php $images = get_sub_field('gallery');
														if( $images ): ?>
											        <?php foreach( $images as $image ): ?>
											            <li>
											             	<img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
											            </li>
											        <?php endforeach; ?>
											    </ul>
												<?php endif; ?>

											</div>
											<div class="details">
												<svg version="1.1" id="cross" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 38.974 38.667" enable-background="new 0 0 38.974 38.667" xml:space="preserve">
												<g>
													<circle fill="none" stroke="#58595B" stroke-width="1.5" stroke-miterlimit="10" cx="19.757" cy="19.282" r="17.526"/>
													<line class="vert" fill="none" stroke="#58595B" stroke-width="1.5" stroke-miterlimit="10" x1="19.757" y1="9.971" x2="19.757" y2="29.689"/>
													<line class="horiz" fill="none" stroke="#58595B" stroke-width="1.5" stroke-miterlimit="10" x1="9.898" y1="19.83" x2="29.616" y2="19.83"/>
												</g>
												</svg>
										
												<div class="open">Expand for more details</div>
												<div class="display clear">
													<div class="third">
														<h3>Project:</h3>
														<p><?php the_sub_field('project_title'); ?></p>
														<p class="date">(<?php the_sub_field('project_date'); ?>)</p>
													</div>
													<div class="two-thirds">
														<h3>Client:</h3>
														<p><?php the_sub_field('project_client'); ?></p>

														<h3>Details:</h3>
														<p><?php the_sub_field('project_details'); ?></p>
														<h3>Materials:</h3>
														<p><?php the_sub_field('project_materials'); ?></p>
													</div>
												</div>
											</div>
										</li>
									</ul>
		 
							    </div>
		
							
							   <?php endwhile;
							
							else :
							
							    // no rows found
							
						endif; ?>
					    			
			
								    
					</div>
					<div class="clear">

					    <?php if( have_rows('project') ):
						while ( have_rows('project') ) : the_row();?>

							<div class="third">
							   <a href="#<?php the_sub_field('project_slug'); ?>" class="scrollitem selected">
								   <div class="project-nav">
								   <img class="featured" src="<?php the_sub_field('featured_image'); ?>" />
								   <h3 class="nav-title"><?php the_sub_field('project_title'); ?></h3>
								   </div>
								</a>
							</div>
						
						<?php endwhile;								
						endif; ?>
					</div>
					 
			    </div>
				<div class="post-right half">
					<h1 class="desktop-header"><?php the_title();?></h1>
					<!--<div class="caption">
						<p><?php the_field('caption'); ?></p>
					</div>-->
					<?php the_content();?>
				
				</div>
			               
		    </div>
		<?php endwhile; ?>
		<?php endif; ?>
	</div>
</div>	
		
<?php get_footer(); ?>