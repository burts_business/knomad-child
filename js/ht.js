HT = {
  isTouch :function(){
    return 'ontouchstart' in document.documentElement;
  },
  ready : function(){
    HT.fajax();
    HT.filters();
    HT.sidebar();
    HT.newsletter();
    if ($('.intro').length){
      if($(window).width() &&!HT.isTouch()){//HANDLE FULL BLEED VIDEO
        HT.intro.setup();
      }else{
        $('.intro').remove();
        $('header').removeClass('introHeader');
      }
    }
    if ($("body.home").length){
    }
    if ($("body.project").length){
    }
  },
  intro : {
    scrollPoint : 0,
    on : false,
    setup : function(){
      $('.intro').show();
      $(window).scrollTop(0);
      $('footer').addClass('introStatic');
      $('body').css('padding-top',$(window).height() + 'px');
      $('.page.container').addClass('noPad');
      HT.intro.scrollPoint = $(window).height();
      HT.intro.on = true;
      $('.intro-logo').css({'top': (HT.intro.scrollPoint/2 - 21)});
      $('header.introHeader .intro-down').click(function(){
        $('html, body').animate({scrollTop:HT.intro.scrollPoint-65},1000);
      });
      $('.introVideo').css({'min-height': $(window).height()});
      $('.intro-logo img').click(function(){
        $('header.introHeader .intro-down').trigger('click');
      });
    },
    close : function(){
      HT.intro.on = false;
      $('.intro').remove();
      $('.page.container').removeClass('noPad');
      $('body').css({'padding-top': 0});
      $('header').removeClass('introHeader');
      $('footer').removeClass('introStatic');
      $(window).scrollTop(0);
    },
    parallax : function(){
      if ($(window).scrollTop() < HT.intro.scrollPoint){
        $('.introVideo').css({'top': ($(window).scrollTop()/-3)});
      }
    },
    logoScroll : function(){
      var top =(HT.intro.scrollPoint/2 - 21);
        $('.intro-logo').css({'top': (top - $(window).scrollTop())});
    },
    scroll : function(){
      HT.intro.parallax();
      HT.intro.logoScroll();
      if ($(window).scrollTop() < HT.intro.scrollPoint){
        $('.intro').css({'height':  HT.intro.scrollPoint - $(window).scrollTop()});
      }
    },
    resize : function(){
    if($(window).scrollTop() == 0){
      $('.intro-logo').css({'top': ($(window).height()/2 - 21)});
    }
  }
  },

  scroll : function(){ 
    if ($("body.journal") && $('.posts-loader').length){
      HT.checkPosts();
    }
    if (HT.intro.on) {
      HT.intro.scroll();
      if($(window).scrollTop() >= HT.intro.scrollPoint -65){
        HT.intro.close();
      }
    }
  },
  checkPosts: function(){
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $('.posts-loader').offset().top;
    var elemBottom = elemTop + $('.posts-loader').height();

    if((elemBottom <= docViewBottom) && (elemTop - 100 >= docViewTop) && $('.posts-loader').hasClass('inactive')){
      $('.posts-loader').addClass('active').removeClass('inactive');
      HT.loadPosts();
    }
  },
  
  }
};

$(document).ready(function(){
  HT.ready();
});

$(document).scroll(function(){
  HT.scroll();
});
$(window).resize(function(){
  HT.intro.resize();
});